# Bienvenido!
**Registro de estudiante** es una aplicación que fue desarrollada para la materia  tecnologías del software en al carrera Ing. Software. El propósito de la aplicación es poder llevar el registro estudiantil de una universidad, donde se irán guardando los datos para su posterior recuperación a la hora que el estudiante quisiera ver las materias seleccionadas durante su modulo actual.

# Uso

**Dependencia**
- Instalar  [MongooseDB](https://mongoosejs.com/) como gestionador de la base de datos.

**Tecnologías usadas**
- body-parser:^1.18.2,
- ejs: ^2.5.7,
- express: ^4.16.2,
- express-session: ^1.15.6,
- mongoose: ^5.0.4,
- morgan: ^1.9.0,
- passport: ^0.4.0,

Para hacer uso de la aplicación, solo descarga el repositorio y hacer doble click en el ejecutador llamado **registroestudianteswin.exe**, este repositorio tiene la version para windows 7 en adelante.

**Desarrolladores**

1. Descargar repositorio
2. `cd ./registroestudianteswin-win32-x64/resources/app`
3. `yarn` o cualquier otro gestionador de paquete que se tenga instalado como 'npm'
4. `yarn start` o `electron-forge start`